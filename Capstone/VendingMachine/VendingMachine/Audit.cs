﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace VendingMachine
{
    class Audit
    {
        static StreamWriter sw;
        public static void LogActivityToFile(string logMessage)
        {
            try
            {
                string directory = Environment.CurrentDirectory;
                string filename = "Log.txt";
                string fullPath = Path.Combine(directory, filename);

                if (sw == null)
                {
                    sw = new StreamWriter(fullPath, true);
                }
                sw.WriteLine(logMessage);
                sw.Flush();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to write log message to file");
            }
        }

    }
}
