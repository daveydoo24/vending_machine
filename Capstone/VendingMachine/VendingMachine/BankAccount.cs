﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendingMachine
{
    public class BankAccount
    {
        public decimal VendingMachineBalance { get; private set; } = 0.0M;
        public decimal CustomerBalance { get; private set; } = 0.0M;
        public string AvailableCustomerBalanceForDisplay
        {
            get
            {
                return $"Your current balance is: {CustomerBalance:C2}";
            }
        }

        public void CustomerLoadFundsToMakePurchase()
        {
            bool isCustomerLoadFundsComplete = false;
            while (!isCustomerLoadFundsComplete)
            {
                FeedMoney();
                if (CustomerInputs.IsFeedMoneyComplete())
                {
                    isCustomerLoadFundsComplete = true;
                }
            }
        }

        public void FeedMoney()
        {
            bool isFeedMoneyComplete = false;
            while (!isFeedMoneyComplete)
            {
                decimal customerCashInput = CustomerInputs.GetCustomerMoneyInput();
                if (IsMoneyInputAmountValid(customerCashInput))
                {
                    AddFundsToCustomerBalance(customerCashInput);
                    DisplayOutputs.DisplayCurrentCustomerBalance(AvailableCustomerBalanceForDisplay);
                    isFeedMoneyComplete = true;
                }
                else
                {
                    Console.WriteLine("Invalid input amount");
                }
            }
        }

        public static bool IsMoneyInputAmountValid(decimal moneyInput)
        {
            bool isValidInput = false;
            if (moneyInput == 1.0M || moneyInput == 2.0M || moneyInput == 5.0M || moneyInput == 10.0M)
            {
                isValidInput = true;
            }
            return isValidInput;
        }

        public void AddFundsToCustomerBalance(decimal cashInput)
        {
            CustomerBalance += cashInput;
            CreateLogEntryForFeedMoney(cashInput);
        }

        public void CreateLogEntryForFeedMoney(decimal amountDeposited)
        {
            string dateTimeStamp = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss tt");
            string logMessage = $"{dateTimeStamp} FEED MONEY: {amountDeposited:C2} {CustomerBalance:C2}";
            Audit.LogActivityToFile(logMessage);
        }

    }

}
