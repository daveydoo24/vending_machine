﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendingMachine
{
    abstract class CustomerInputs
    {
        public static string GetCustomerInputFromMenu()
        {
            bool isInputValid = false;
            string customerInput = "";
            while (!isInputValid)
            {
                Console.Write("Please enter your selection: ");
                customerInput = Console.ReadLine();
                if (customerInput == "1" || customerInput == "2" || customerInput == "3")
                {
                    isInputValid = true;
                }
                else
                {
                    Console.WriteLine("Invalid input - please re-enter your selection: ");
                }
            }
            return customerInput;
        }

        public static decimal GetCustomerMoneyInput()
        {
            decimal customerCash = 0.0M;
            bool isInputValid = false;

            while (!isInputValid)
            {
                Console.WriteLine("\nVendo-Matic 800 accepts $1, $2, $5, or $10!");
                Console.Write("\nPlease feed your money: ");
                string customerInput = Console.ReadLine();
                if (!Decimal.TryParse(customerInput, out customerCash))
                {
                    Console.WriteLine("Invalid input - please try again!");
                }
                else
                {
                    isInputValid = true;
                }
            }
            return customerCash;
        }

        public static bool IsFeedMoneyComplete()
        {
            bool isCustomerFinishedFeedingMoney = false;
            Console.Write("Are you finished entering funds? Please enter (y) or (n): ");
            string customerResponse = Console.ReadLine();
            if (customerResponse.Substring(0, 1).ToLower() == "y")
            {
                isCustomerFinishedFeedingMoney = true;
            }
            return isCustomerFinishedFeedingMoney;
        }
    }
}
