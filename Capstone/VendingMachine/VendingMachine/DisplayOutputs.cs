﻿using System;
using System.Collections.Generic;
using System.Text;
using VendingMachine.InventoryClasses;

namespace VendingMachine
{
    abstract class DisplayOutputs
    {
        public static void DisplayMainMenu()
        {
            Console.WriteLine("Welcome To Vendo-Matic 5000!\n");
            Console.WriteLine("Please choose from the following menu options:");
            Console.WriteLine("(1) Display Vending Machine Items");
            Console.WriteLine("(2) Purchase Item");
            Console.WriteLine("(3) Exit Vendo-Matic 5000\n");
        }

        public static void DisplayPurchaseMenu()
        {
            Console.WriteLine("\nVendo-Matic 5000 Purchase Menu");
            Console.WriteLine("Please choose from the following menu options:");
            Console.WriteLine("(1) Feed Money");
            Console.WriteLine("(2) Select Product");
            Console.WriteLine("(3) Finish Transaction\n");
        }

        public static void DisplayInventoryItems(List<string> inventoryItemsForDisplay)
        {
            Console.WriteLine();
            foreach (string item in inventoryItemsForDisplay)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

        public static void DisplayCurrentCustomerBalance(string balanceToBeDisplayed)
        {
            Console.WriteLine(balanceToBeDisplayed);
        }
    }
}
