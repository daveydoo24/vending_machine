﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using VendingMachine.InventoryClasses;

namespace VendingMachine.FileImport
{
    public abstract class InventoryImport
    {
        public static Dictionary<string, InventoryItem> CreateNewVendingMachineInventory()
        {
            List<string> rawInventoryFile = ImportNewInventoryFile();
            Dictionary<string, InventoryItem> newVendingMachineInventory = LoadVendingMachineInventoryFromFileImport(rawInventoryFile);
            return newVendingMachineInventory;
        }

        public static List<string> ImportNewInventoryFile()
        {
            string directory = Environment.CurrentDirectory;
            string filename = "vendingmachine.csv";
            string fullPath = Path.Combine(directory, filename);

            List<string> inventoryItems = new List<string>();

            try
            {
                using (StreamReader sr = new StreamReader(fullPath))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        inventoryItems.Add(line);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error reading from file!");
            }
            return inventoryItems;
        }

        public static Dictionary<string, InventoryItem> LoadVendingMachineInventoryFromFileImport(List<string> inventoryFileData)
        {
            Dictionary<string, InventoryItem> inventoryItems = new Dictionary<string, InventoryItem>();

            foreach (string line in inventoryFileData)
            {
                string[] itemValues = line.Split('|');
                string itemSlot = itemValues[0];
                string itemName = itemValues[1];
                decimal itemPrice = decimal.Parse(itemValues[2]);
                string itemType = itemValues[3].ToLower();

                if (itemType == "chip")
                {
                    Chip chip = new Chip(itemSlot, itemName, itemPrice);
                    inventoryItems[itemSlot] = chip;
                }
                else if (itemType == "gum")
                {
                    Gum gum = new Gum(itemSlot, itemName, itemPrice);
                    inventoryItems[itemSlot] = gum;
                }
                else if (itemType == "candy")
                {
                    Candy candy = new Candy(itemSlot, itemName, itemPrice);
                    inventoryItems[itemSlot] = candy;
                }
                else if (itemType == "drink")
                {
                    Drink drink = new Drink(itemSlot, itemName, itemPrice);
                    inventoryItems[itemSlot] = drink;
                }
            }
            return inventoryItems;
        }
    }
}
