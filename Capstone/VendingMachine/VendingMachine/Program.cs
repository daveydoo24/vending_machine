﻿using System;
using VendingMachine.InventoryClasses;
using System.Collections.Generic;
using VendingMachine.FileImport;

namespace VendingMachine
{
    class Program
    {
        static void Main(string[] args)
        {
            VendingMachine VendoMatic = new VendingMachine();
            bool isTransactionComplete = false;

            while (!isTransactionComplete)
            {
                isTransactionComplete = VendoMatic.MainVendingMachineOperationControl();
            }
            Console.WriteLine("\nThank you for using Vendo-Matic 5000!");
        }
    }
}
