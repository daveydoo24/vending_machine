﻿using System;
using System.Collections.Generic;
using System.Text;
using VendingMachine.InventoryClasses;
using VendingMachine.FileImport;

namespace VendingMachine
{
    public class VendingMachine
    {
        public Dictionary<string, InventoryItem> InventoryItems { get; set; } = new Dictionary<string, InventoryItem>();

        public BankAccount VendingMachineBankAccount { get; set; } = new BankAccount();

        public List<string> AvailableInventoryForDisplay
        {
            get
            {
                return CreateInventoryDisplayItems(InventoryItems);
            }
        }

        public VendingMachine()
        {
            InventoryItems = InventoryImport.CreateNewVendingMachineInventory();
        }

        public bool MainVendingMachineOperationControl()
        {
            bool isOperationComplete = false;
            DisplayOutputs.DisplayMainMenu();
            string customerInput = CustomerInputs.GetCustomerInputFromMenu();

            if (customerInput == "1")
            {
                DisplayOutputs.DisplayInventoryItems(AvailableInventoryForDisplay);
            }
            else if (customerInput == "2")
            {
                PurchaseMenu();
            }
            else
            {
                isOperationComplete = true;
            }
            return isOperationComplete;
        }


        public void PurchaseMenu()
        {
            bool isCustomerFinishedWithPurchase = false;
            while (!isCustomerFinishedWithPurchase)
            {
                List<string> inventoryItemsToBeDisplayed = CreateInventoryDisplayItems(InventoryItems);

                DisplayOutputs.DisplayPurchaseMenu();
                string customerMenuSelection = CustomerInputs.GetCustomerInputFromMenu();

                if (customerMenuSelection == "1")
                {
                    VendingMachineBankAccount.CustomerLoadFundsToMakePurchase();
                }
                else if (customerMenuSelection == "2")
                {
                    // SELECT PRODUCT
                    Console.WriteLine("select product");
                }
                else
                {
                    // FINISH TRANSACTION
                    Console.WriteLine("Finish Transaction");
                    isCustomerFinishedWithPurchase = true;
                }
            }
        }

        public List<string> CreateInventoryDisplayItems(Dictionary<string, InventoryItem> currentInventory)
        {
            List<string> inventoryItemsForDisplay = new List<string>();
            foreach (KeyValuePair<string, InventoryItem> item in currentInventory)
            {
                string singleItemForDisplay = $"{item.Key} {item.Value.Slot} {item.Value.Name} {item.Value.Price} {item.Value.Quantity:C2}";
                inventoryItemsForDisplay.Add(singleItemForDisplay);
            }
            return inventoryItemsForDisplay;
        }
    }
}
